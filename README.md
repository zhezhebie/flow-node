# flowNode

#### 介绍
自定义流程节点组件，您可以用它实现自定义流程、组织架构等等

#### 演示站点
https://flow.17xunle.com
<br>
账号：13600000000
<br>
密码：admin123456

#### 您可以实现类似效果
##### 效果1
![输入图片说明](uploads/flowNodes1657792524386.jpg).png)
##### 效果2
![输入图片说明](uploads/flowNodesimage.png)

以及自定义节点的内容
#### 使用说明

1.  引入模块
```
    layui.config({
        base: './lib/layui_exts/',
    }).extend({
        flowNode: "flowNode",
    }).use(['flowNode'], function () {
           let flowNode = layui.flowNode
    });
```
2.  在指定元素内输出页面
```
    let flowNodeObj = flowNode.render({
        el:'#flow_nodes',//要输出到的目标元素
        data:[],//节点数据
        nodeContent: function(node){
            //参数为当前节点数据
            return node.title
        },//自定义节点内容
    });
```

3.  刷新
```
    flowNodeObj.reload(nodes)
```
#### 数据结构说明
```
[
    {"id":1,
                "pid":0,//父级节点id
                "title":"开始",//节点标题，可自定义
                "type":1,//节点类型：1开始节点，2结束节点（舍弃），3普通节点，4收敛节点
                "children":[],//子节点，决定节点的分支情况，每个子节点结构与此相同
                "convergent":{},//收敛节点，决定了多分支的合并效果，一个节点只能包含一个收敛节点，结构与此相同
    }
]
```
#### 数据结构大体思路
重要一点，将收敛节点父级设置与分支开始节点的父级一致，其他后续补充...

#### 数据结构示例
##### 效果1数据结构
```
[
            {
                "id":1,
                "flow_id":1,
                "pid":0,
                "title":"开始",
                "type":1,
                "created_at":"2022-07-09 07:49:40",
                "updated_at":"2022-07-09 07:49:40",
                "sort":1,
                "handler_id":0,
                "aging":0,
                "desc":"",
                "children":[
                    {
                        "id":2,
                        "flow_id":1,
                        "pid":1,
                        "title":"节点1-1",
                        "type":3,
                        "created_at":"2022-07-09 07:49:56",
                        "updated_at":"2022-07-09 07:50:18",
                        "sort":0,
                        "handler_id":0,
                        "aging":0,
                        "desc":"",
                        "children":[
                            {
                                "id":9,
                                "flow_id":1,
                                "pid":2,
                                "title":"节点1-1-1",
                                "type":3,
                                "created_at":"2022-07-14 09:31:37",
                                "updated_at":"2022-07-14 09:31:37",
                                "sort":0,
                                "handler_id":0,
                                "aging":0,
                                "desc":"",
                                "children":[

                                ],
                                "convergent":null
                            },
                            {
                                "id":10,
                                "flow_id":1,
                                "pid":2,
                                "title":"节点1-1-2",
                                "type":3,
                                "created_at":"2022-07-14 09:31:53",
                                "updated_at":"2022-07-14 09:31:53",
                                "sort":0,
                                "handler_id":0,
                                "aging":0,
                                "desc":"",
                                "children":[

                                ],
                                "convergent":null
                            }
                        ],
                        "convergent":null
                    },
                    {
                        "id":3,
                        "flow_id":1,
                        "pid":1,
                        "title":"节点1-2",
                        "type":3,
                        "created_at":"2022-07-09 07:50:10",
                        "updated_at":"2022-07-09 07:50:10",
                        "sort":0,
                        "handler_id":0,
                        "aging":0,
                        "desc":"",
                        "children":[
                            {
                                "id":5,
                                "flow_id":1,
                                "pid":3,
                                "title":"节点1-2-1",
                                "type":3,
                                "created_at":"2022-07-09 07:50:53",
                                "updated_at":"2022-07-09 07:50:53",
                                "sort":0,
                                "handler_id":0,
                                "aging":0,
                                "desc":"",
                                "children":[

                                ],
                                "convergent":null
                            },
                            {
                                "id":6,
                                "flow_id":1,
                                "pid":3,
                                "title":"节点1-2-2",
                                "type":3,
                                "created_at":"2022-07-09 07:51:04",
                                "updated_at":"2022-07-09 07:51:04",
                                "sort":0,
                                "handler_id":0,
                                "aging":0,
                                "desc":"",
                                "children":[

                                ],
                                "convergent":null
                            }
                        ],
                        "convergent":{
                            "id":7,
                            "flow_id":1,
                            "pid":3,
                            "title":"节点1-2-3",
                            "type":4,
                            "created_at":"2022-07-09 07:51:24",
                            "updated_at":"2022-07-09 07:51:24",
                            "sort":0,
                            "handler_id":0,
                            "aging":0,
                            "desc":"",
                            "children":[
                                {
                                    "id":8,
                                    "flow_id":1,
                                    "pid":7,
                                    "title":"节点2-1",
                                    "type":3,
                                    "created_at":"2022-07-14 09:30:54",
                                    "updated_at":"2022-07-14 09:30:54",
                                    "sort":0,
                                    "handler_id":0,
                                    "aging":0,
                                    "desc":"",
                                    "children":[

                                    ],
                                    "convergent":null
                                }
                            ],
                            "convergent":null
                        }
                    }
                ],
                "convergent":{
                    "id":4,
                    "flow_id":1,
                    "pid":1,
                    "title":"结束",
                    "type":4,
                    "created_at":"2022-07-09 07:50:36",
                    "updated_at":"2022-07-14 09:30:33",
                    "sort":0,
                    "handler_id":0,
                    "aging":0,
                    "desc":"",
                    "children":[

                    ],
                    "convergent":null
                }
            }
        ]
```